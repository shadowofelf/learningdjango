import os
from celery import Celery
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "my_syte.settings")

app = Celery('my_syte')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

# timeout for celery beat from env var CL_TIMEOUT
timeout = float(settings.CELERY_TIMEOUT)

# celery beat task for cyclic repetition
app.conf.beat_schedule = {
    "creating_new_objects_of_news": {
        'task': 'news_feed.tasks.create_new_news',
        'schedule': timeout,
    }
}
