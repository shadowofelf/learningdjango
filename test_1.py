import os
import django
from news_feed.models import News
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "my_syte.settings")
django.setup()


def run():
    count = 4
    news_list = News.objects.all()
    for new in news_list:
        count += 1
        new.news_id = count
        new.save()


if __name__ == "__main__":
    run()
