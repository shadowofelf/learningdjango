# from .views import api_news, api_news_detail
from api.views import APIComments, APINews, APINewsDetail, APICommentsDetail, APITag
from django.urls import path

app_name = 'api'
urlpatterns = [
    path('v1/news', APINews.as_view(), name='news'),
    path('v1/news/<int:pk>', APINewsDetail.as_view(), name='news_detail'),
    path('v1/comments/<int:pk>', APIComments.as_view(), name='news_comments'),
    path('v1/comments/<int:pk>',
         APICommentsDetail.as_view(), name='comment_detail'),
    path('v1/news/<int:pk>/tags', APITag.as_view(), name='news_tags'),
]
