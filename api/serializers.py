from rest_framework import serializers
from news_feed.models import News, Comment
from django.contrib.auth.models import User
from tags.models import Tag


class NewsSerializer(serializers.ModelSerializer):
    comments = serializers.HyperlinkedRelatedField(
        view_name='api:news_comments',
        many=True,
        read_only=True
    )

    class Meta:
        model = News
        fields = ['id', 'news_title', 'news_source',
                  'news_pub_date', 'news_previos_text',
                  'news_source_date', 'news_text', 'comments']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'id', 'email',)


class CommentSerializer(serializers.ModelSerializer):
    user = serializers.CharField(read_only=True, source='comm_author.username')

    class Meta:
        model = Comment
        fields = ('id', 'comm_text', 'user',)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'tag_ru')
