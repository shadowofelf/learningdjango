
from news_feed.permissions import IsOwnerOrReadOnly

from rest_framework import permissions
from django.contrib.contenttypes.models import ContentType
from news_feed.models import Comment, News
from tags.models import Tag
from .serializers import CommentSerializer, NewsSerializer, TagSerializer
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly, IsAuthenticated
from rest_framework import generics


class APINews(generics.ListCreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = (IsAuthenticated, )


class APINewsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)


class APIComments(generics.ListCreateAPIView):
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    def get_queryset(self):
        return Comment.objects.filter(news=self.kwargs.get('pk'))


class APICommentsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]


class APITag(generics.ListCreateAPIView):
    serializer_class = TagSerializer
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]

    def get_queryset(self):
        news_ct = ContentType.objects.get_for_model(News)
        pk = self.kwargs.get('pk')
        tags_news = Tag.objects.distinct().filter(
            taggeditem__content_type=news_ct,
            taggeditem__object_id=pk)
        return tags_news

# @api_view(['GET', 'POST'])
# def api_news(request):
#     if request.method == 'GET':
#         news = News.objects.all()
#         serializer = NewsSerializer(news, many=True)
#         return Response(serializer.data)
#     elif request.method == 'POST':
#         data = request.data
#         print(data)
#         serializer = NewsSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors,
#                         status=status.HTTP_400_BAD_REQUEST)
# @api_view(['GET', 'PATCH', 'PUT', 'DELETE'])
# def api_news_detail(request, pk):
#     new_detail = News.objects.get(pk=pk)
#     if request.method == 'GET':
#         serializer = NewsSerializer(new_detail)
#         return Response(serializer.data)
#     elif request.method == 'PUT' or request.method == 'PATCH':
#         serializer = NewsSerializer(new_detail, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
#     elif request.method == 'DELETE':
#         new_detail.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
#     else:
#         return Response(status=status.HTTP_400_BAD_REQUEST)
