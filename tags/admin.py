from django.contrib import admin
from .models import TaggedItem, Tag
# Register your models here.


class TagsItemsAdmin(admin.ModelAdmin):
    fieldsets = [
        # ('Main',{'fields': ['id']}),
        ('Connect', {'fields': ['object_id', 'content_type', 'tag_id']}),
    ]

    list_display = ('id',)
    ordering = ('-id',)


admin.site.register(TaggedItem, TagsItemsAdmin)


class TagsAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Main', {'fields': ['tag', 'tag_ru', 'tag_en']}),
    ]

    list_display = ('id', 'tag', 'tag_ru')
    list_display_links = ('id', 'tag')
    ordering = ('-tag',)


admin.site.register(Tag, TagsAdmin)
