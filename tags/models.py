from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
# Create your models here.


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    tag = models.SlugField()
    tag_ru = models.CharField(max_length=50)
    tag_en = models.CharField(max_length=50)

    def __str__(self):
        return self.tag


class TaggedItem(models.Model):
    id = models.AutoField(primary_key=True)
    tag_id = models.ForeignKey(Tag, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
