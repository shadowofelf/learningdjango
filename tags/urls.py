from django.urls import path
from . import views

app_name = 'tags'
urlpatterns = [
    path('', views.IndexViews.as_view(), name='index'),
    path('news/<slug:tag>/', views.TaggedObj.as_view(), name='tagged')
]
