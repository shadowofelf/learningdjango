from django.contrib.contenttypes.models import ContentType
from django.views import generic
# Create your views here.
from news_feed.models import News
from .models import Tag


class IndexViews(generic.ListView):
    template_name = 'tags/index.html'
    context_object_name = 'obj_list'

    def get_queryset(self, tag_name='World', model=News):
        tag_obj = Tag.objects.get(tag=tag_name)
        content_type_model = ContentType.objects.get_for_model(model)
        tagged_obj = tag_obj.taggeditem_set.filter(content_type=content_type_model)
        obj_pool = []
        for data in tagged_obj:
            obj_pool.append(data.content_object)
        return obj_pool


class TaggedObj(generic.DetailView):
    model = Tag
    context_object_name = "obj_list"
    template_name = 'tags/index.html'

    def get_object(self, queryset=None):
        tag_obj = Tag.objects.get(tag=self.kwargs.get('tag'))
        content_type_news = ContentType.objects.get_for_model(News)
        tagged_obj = tag_obj.taggeditem_set.filter(content_type=content_type_news)
        obj_pool = []
        for data in tagged_obj:
            obj_pool.append(data.content_object)
        return obj_pool
