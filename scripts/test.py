from news_feed.models import News


def run():
    count = 0
    news_list = News.objects.all()
    for new in news_list:
        new.id = count
        count += 1
        new.save()
