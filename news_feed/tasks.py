from celery import shared_task
from .scraping import rt_scraping


@shared_task
def create_new_news():
    rt_scraping.rt_news_add()
