from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from tags.models import TaggedItem
from django.contrib.auth.models import User


class News(models.Model):
    id = models.AutoField(primary_key=True)
    news_title = models.CharField(max_length=200)
    news_previos_text = models.TextField(null=True)
    news_text = models.TextField()
    news_pub_date = models.DateTimeField('Date published', auto_now_add=True)
    news_source_date = models.DateTimeField()
    news_source = models.CharField(max_length=200)
    news_site_id = models.CharField(max_length=100, null=True)
    news_tags = GenericRelation(TaggedItem)

    class Meta:
        ordering = ('-news_source_date',)

    def short_source_name(self):
        if self.news_source.find('http') != -1:
            short_name = self.news_source[:self.news_source.find('/', 8)]
        else:
            short_name = 'None'
        return short_name

    def __str__(self):
        return self.news_title


class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    comm_author = models.ForeignKey(User, on_delete=models.CASCADE)
    comm_text = models.TextField()
    pub_date = models.DateTimeField('Date published', auto_now_add=True)
    news = models.ForeignKey(News, on_delete=models.CASCADE,
                             related_name='comments')

    def __str__(self):
        return self.comm_text
