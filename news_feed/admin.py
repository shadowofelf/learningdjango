from django.contrib import admin
from .models import News, Comment
from tags.models import TaggedItem
from django.contrib.contenttypes.admin import GenericTabularInline
from django_celery_beat.models import IntervalSchedule, ClockedSchedule, SolarSchedule, CrontabSchedule, PeriodicTask
from django_celery_results.models import GroupResult
# Register your models here.


class TagsItensInline(GenericTabularInline):
    extra = 1
    model = TaggedItem


class CommentsInLine(admin.StackedInline):
    model = Comment
    extra = 1


class NewsAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Main', {'fields': ['news_title', 'news_source', 'news_site_id']}),
        ('Text', {'fields': ['news_previos_text', 'news_text']}),
        ('Dates', {'fields': ['news_source_date']}),
    ]
    inlines = [TagsItensInline, CommentsInLine]
    list_display = ('id', 'news_title', 'news_source', 'news_source_date')
    list_display_links = ('id', 'news_title')
    ordering = ('-news_source_date',)
    list_filter = ['news_source_date']
    search_fields = ['news_title', 'news_text']


class CommentAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Main', {'fields': ['comm_author', 'comm_text']}),
    ]

    list_display = ('id', 'comm_author', 'pub_date', 'news')
    ordering = ('-pub_date',)


admin.site.register(News, NewsAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.unregister(IntervalSchedule)
admin.site.unregister(ClockedSchedule)
admin.site.unregister(SolarSchedule)
admin.site.unregister(GroupResult)
admin.site.unregister(CrontabSchedule)
admin.site.unregister(PeriodicTask)
