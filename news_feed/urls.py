from django.urls import path
from . import views

app_name = 'news_feed'
urlpatterns = [
    # main page
    path('', views.IndexView.as_view(), name='index'),
    # page of details of news
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:news_id>/comment', views.comment, name='comment'),
    path('<int:news_id>/delete/<int:comm_id>',
         views.delete_comment, name='delete'),
    path('register/', views.UserRegister.as_view(), name='register'),
    path('auth/', views.UserLogin.as_view(), name='login'),
]
