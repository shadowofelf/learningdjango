import requests
import locale
import pytz
from pprint import pprint
from lxml import etree
from lxml.html import fromstring
from datetime import datetime, timedelta
from django.utils.timezone import make_aware
from tags.models import Tag, TaggedItem
from ..models import News
from googletrans import Translator

locale.setlocale(locale.LC_ALL, '')


def download(url, user_agent='Chrome/42.0.2311.135',
             charset='utf-8', num_retries=2):
    '''
    Method for download html from news url. Using request method
    and have some set of incoming variables
    Incoming variables:
        - url - url of news
        - user_agent- id of emulating client app, default Chrome/42.0.2311.135
        - charset - charset of page, default utf-8
        - num_retries - num retries after break download
        (if the method returned an error on the first attemption) default =2
    '''
    headers = {'User-agent': user_agent}
    try:
        response = requests.get(url, headers=headers)
        html = response.text
        if response.status_code > 400:
            print('Download error:', html)
            html = None
            if num_retries and 500 <= response.status_code < 600:
                return download(url, num_retries=num_retries - 1)
    except requests.exceptions.RequestException as e:
        print('Download error:', e)
        html = None
    return html


def xprint(element):
    '''
    Method for testing xpath data. Only for dev
    Input variables: xpath element with string inside
    Output data: string from xpath element
    '''
    return etree.tostring(element, encoding='unicode')


def parse_link(url):
    '''
    Method for forming a url string. From incoming url deleting '/' in the end
    and deleting part after main page text (russian.rt.com)
    Incoming variables:  url of news pool
    Output data: string of parsed url.
    '''
    if url[len(url) - 1] == '/':
        parse_url = url[:len(url) - 1]
    else:
        parse_url = url
    if parse_url.find('http', 0, 4) >= 0:
        parse_url = parse_url[:parse_url.find('/', 8)]
    else:
        parse_url = parse_url[:parse_url.find('/')]
    return parse_url


def text_extract(xpath_element):
    '''
    Method for receving text from xpath data.
    Input variables: element of xpath with text
    Output data: string variables of text data
    '''
    element = xpath_element
    tag_pull = ''
    all_text = element.xpath('.//text()')
    for text in all_text:
        tag_pull += text
    return tag_pull.replace('\xa0', ' ')


def news_data(url):
    '''
    Method for parsing news page for receving data.
    Implemented using thx xpath function
    Input variables: url of news
    Output data: dictionary of receved data. Title, date,
    previus text, main part text, url source, tag(list)
    '''
    html = download(url)
    tree = fromstring(html)
    div_main = tree.xpath('//div[@class="article article_article-page"]')
    for data in div_main:
        title = data.findtext('.//h1[@class="article__heading article__heading_article-page"]')
        date = data.findtext('.//time[@class="date"]').strip()
        text_top_side = text_extract(data.find(
            './div[@class="article__summary article__summary_article-page js-mediator-article"]'))
        text_bot_side = text_extract(data.find(
            './div[@class="article__text article__text_article-page js-mediator-article"]'))
        tag_not_formating = data.xpath('.//a[@rel="tag"]/text()')
        tag = [tag_text.strip() for tag_text in tag_not_formating]
    return {
        'title': title,
        'date': date,
        'text_prev': text_top_side,
        'main_text': text_bot_side,
        'source': url,
        'tag': tag
    }


def take_news_id(last_url_part):
    '''
    Method for receving news id from link news
    Input variables: url of news without main part (https://russian.rt.com)
    Output data: receved News ID
    '''
    news_id = ''
    url = last_url_part[last_url_part.rfind('/') + 1:]
    for ch in url:
        if ch.isnumeric():
            news_id += ch
        else:
            return news_id


def date_transform(date):
    '''
    Method for transform string date in Russian to a datetime format
    in the GMT time zone
    Input variables: date in string format
    ('date month(text format) year, hour:min')
    Output data: date in datetime form in the GMT time zone
    '''
    news_time = datetime.strptime(date, '%d %B %Y, %H:%M')
    gmt_time = news_time - timedelta(hours=3)
    gmt_time = make_aware(gmt_time, pytz.timezone('GMT'))
    return gmt_time


def tags_adding(tags, news_id):
    '''
    Method for adding tags to the news
    Incoming variables: set of tags, news id
    Output data: writing data to the BD
    '''
    print(news_id)
    if tags:
        for tag in tags:
            tag_rus = tag.lower()
            if not Tag.objects.filter(tag_ru__iexact=tag_rus):
                trans = Translator()
                tag = trans.translate(tag, src='ru', dest='en').text.lower()
                Tag.objects.create(tag=tag.replace(" ", "_"), tag_ru=tag_rus, tag_en=tag)
            tag_obj = Tag.objects.get(tag_ru=tag_rus)
            news_obj = News.objects.get(news_site_id=news_id)
            tagged_obj = TaggedItem(content_object=news_obj, tag_id=tag_obj)
            tagged_obj.save()


def news_pull(url):
    '''
    Method for receving url pool of news and processing this url for taking data.
    After receiving data, thay are writing to the database
    Icoming variables: url of pool RT news
    Output data: Count of writen news
    '''
    news_aded_count = 0
    html = download(url)
    tree = fromstring(html)
    a_tag = tree.xpath('//a[@class="link link_color "]/@href')
    for link in reversed(a_tag):
        news_url = parse_link(url) + link
        new_site_id = take_news_id(link)
        news_created = News.objects.filter(news_site_id=new_site_id)
        if news_url.find('russian.rt.com/sport/') == -1 and not news_created:
            news_aded_count += 1
            data = news_data(news_url)
            News.objects.create(
                news_title=data['title'],
                news_text=data['main_text'],
                news_previos_text=data['text_prev'],
                news_source_date=date_transform(data['date']),
                news_source=data['source'],
                news_site_id=new_site_id,
            )
            print(new_site_id)
            tags_adding(data['tag'], new_site_id)
    return news_aded_count


def rt_news_add():
    '''
    main method for starting scraping RT page.
    Does not have incoming variables
    Does not hace output data
    '''
    count = news_pull('https://russian.rt.com/listing/type.News.tag.novosty-glavnoe/prepare/all-news/15/0')
    print(count, ' news added')


if __name__ == '__main__':
    newss = news_pull('https://russian.rt.com/listing/type.News.tag.novosty-glavnoe/prepare/all-news/15/0')
    pprint((newss))
