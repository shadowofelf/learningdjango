from django.core.management.base import BaseCommand, CommandError
from news_feed.models import News

class Command(BaseCommand):
    '''
    upd BD after migration 003
    '''
    def handle(self, *args, **options):
        count = 2
        news_list = News.objects.all()
        for new in news_list:        
            count += 1
            new.news_id = count
            new.save()
        self.stdout.write('command complite')