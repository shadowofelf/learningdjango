from django.test import TestCase
from .models import News
from django.urls import reverse
# Create your tests here.


def create_news(news_title, news_text, news_pub_date, news_source):
    """
    create new news
    """
    return News.objects.create(news_title=news_title,
                               news_text=news_text,
                               news_pub_date=news_pub_date,
                               news_source=news_source)


class NewsIndexViewTests(TestCase):
    def test_no_news(self):
        '''
        if no news exists. n appropriate message is displayed.
        '''
        response = self.client.get(reverse('news_feed:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No news now')
        self.assertQuerysetEqual(response.context['news_list'], [])

    def test_new_news(self):
        '''
        test with one news create
        '''
        create_news(
            news_title="now",
            news_text='this is text',
            news_pub_date='2021-01-24 22:36',
            news_source='vk.com',
        )
        response = self.client.get(reverse('news_feed:index'))
        self.assertQuerysetEqual(response.context['news_list'],
                                 ['<News: now>'])
